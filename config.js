module.exports = {
    "extends": ["group:monorepos", "group:recommended", "workarounds:all"],
    "endpoint": "https://gitlab.com/api/v4/",
    "platform": "gitlab",
    "autodiscover": true,
    "autodiscoverFilter": "opendurak/*",
    "pinDigests": true,
    "labels": ["renovate"],
    "requireConfig": "optional",
    "automerge": true,
    "rebaseWhen": "behind-base-branch",
    "configMigration": true,
    "recreateClosed": true,
    "packageRules": [
        {
            "matchPackageNames": ["openjdk"],
            "matchUpdateTypes": ["major"],
            "enabled": false
        }
    ]
};
